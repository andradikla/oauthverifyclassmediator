package com.kla.test.OauthVerifyClassMediator;

import java.time.Duration;
import java.util.Map;

import org.apache.axis2.context.ConfigurationContext;
import org.apache.http.HttpHeaders;
import org.apache.synapse.MessageContext;
import org.apache.synapse.SynapseLog;
import org.apache.synapse.core.axis2.Axis2MessageContext;
import org.apache.synapse.mediators.AbstractMediator;
import org.wso2.carbon.identity.oauth2.stub.dto.OAuth2TokenValidationRequestDTO_OAuth2AccessToken;

import com.okta.jwt.AccessTokenVerifier;
import com.okta.jwt.Jwt;
import com.okta.jwt.JwtVerificationException;
import com.okta.jwt.JwtVerifiers;

public class OktaTokenVerifyClass extends AbstractMediator{

	private String jwtString;
	private ConfigurationContext configContext;
	private static final String CONSUMER_KEY_HEADER = "Bearer";
	private static final String OAUTH_HEADER_SPLITTER = ",";
	private static final String CONSUMER_KEY_SEGMENT_DELIMITER = " ";
	
	public boolean mediate(MessageContext synCtx) {
		
		//Object body= synCtx.getEnvelope().getBody();
		 //synCtx.setProperty("REQ", body);
		String issuerUrl ="https://dev-595942.oktapreview.com/oauth2/default";
	    String audience  = "api://default";
	       
		SynapseLog synLog = getLog(synCtx);

		System.out.println("debug enabled  >>>>>>>>> "+synLog.isTraceOrDebugEnabled());

        if (synLog.isTraceOrDebugEnabled()) {
            synLog.traceOrDebug("Start : JWTDecoder mediator");
            if (synLog.isTraceTraceEnabled()) {
                synLog.traceTrace("Message : " + synCtx.getEnvelope());
            }
        }
        
		
        String jwtString = getAccessTokenString(synCtx);
        	try {
			        AccessTokenVerifier jwtVerifier = JwtVerifiers.accessTokenVerifierBuilder()
			                                    .setIssuer(issuerUrl)
			                                    .setAudience(audience)
			                                    .setConnectionTimeout(Duration.ofSeconds(1)) // defaults to 1000ms
			                                    .setReadTimeout(Duration.ofSeconds(1))       // defaults to 1000ms
			                                    .build();
			          Jwt jwt= jwtVerifier.decode(jwtString);
			          
			        System.out.println(jwt.getTokenValue()); // print the token
					System.out.println(jwt.getClaims().get("invalidKey")); // an invalid key just returns null
				    System.out.println(jwt.getClaims().get("groups")); // handle an array value
					System.out.println(jwt.getExpiresAt()); // print the expiration time
					        
					   return true;
					} catch (JwtVerificationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						return false;
					}
	        catch(Exception e) {
	        	e.printStackTrace();
	        	return false;
	        }
	}
	private String getAccessTokenString(MessageContext msgCtx) {
		Map headers = (Map) ((Axis2MessageContext) msgCtx).getAxis2MessageContext().
                getProperty(org.apache.axis2.context.MessageContext.TRANSPORT_HEADERS);
		
		System.out.println(((Axis2MessageContext) msgCtx).getAxis2MessageContext().getAxisMessage());
		
        String apiKey = null;
        if (headers != null) {
            apiKey = extractCustomerKeyFromAuthHeader(headers);
        
            System.out.println(apiKey);
        }
       return apiKey;
	}
	
	private String extractCustomerKeyFromAuthHeader(Map headersMap) {
        //From 1.0.7 version of this component onwards remove the OAuth authorization header from
        // the message is configurable. So we dont need to remove headers at this point.
        String authHeader = (String) headersMap.get(HttpHeaders.AUTHORIZATION);
        if (authHeader == null) {
            return null;
        }
        if (authHeader.startsWith("OAuth ") || authHeader.startsWith("oauth ")) {
            authHeader = authHeader.substring(authHeader.indexOf("o"));
        }
        String[] headers = authHeader.split(OAUTH_HEADER_SPLITTER);
        if (headers != null) {
            for (String header : headers) {
                String[] elements = header.split(CONSUMER_KEY_SEGMENT_DELIMITER);
                if (elements != null && elements.length > 1) {
                    boolean isConsumerKeyHeaderAvailable = false;
                    for (String element : elements) {
                        if (!"".equals(element.trim())) {
                            if (CONSUMER_KEY_HEADER.equals(element.trim())) {
                                isConsumerKeyHeaderAvailable = true;
                            } else if (isConsumerKeyHeaderAvailable) {
                                return removeLeadingAndTrailing(element.trim());
                            }
                        }
                    }
                }
            }
        }
        return null;
    }
	
    private String removeLeadingAndTrailing(String base) {
        String result = base;
        if (base.startsWith("\"") || base.endsWith("\"")) {
            result = base.replace("\"", "");
        }
        return result.trim();
    }


}
